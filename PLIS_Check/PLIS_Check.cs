﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Configuration;
using PLIS_Check.Library;

namespace PLIS_Check
{
    public partial class PLIS_Check : ServiceBase
    {
        
        public PLIS_Check(string[] args)
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            FileOperations.checkLogDirectory();
            FileOperations.checkLogStoreDirectory();
            FileOperations.checkConfigDirectory();
            FileOperations.checkUriFile();
            FileOperations.checkEmailFile();
            
            ErrorLog.WriteErrorLog("-----------FILE CREATED-------------");
            
            Timers timer = new Timers();            
            timer.SetTimerPolling(Properties.Settings.Default.PollingInterval);
            timer.SetTimerEmail(Properties.Settings.Default.EmailInterval);
        }

        protected override void OnStop()
        {
            
        }

        internal void TestStartupAndStop(string[] args)
        {
            this.OnStart(args);
            Console.ReadLine();
            this.OnStop();
        }

    }
}
