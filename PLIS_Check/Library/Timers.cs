﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace PLIS_Check.Library
{
    class Timers
    {
        private System.Timers.Timer timerPolling = new System.Timers.Timer();

        public void SetTimerPolling(int interval)
        {                        
            timerPolling.Interval = interval;
            timerPolling.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimerPolling);
            timerPolling.Start();
        }

        public void OnTimerPolling(object sender, System.Timers.ElapsedEventArgs args)
        {
            List<String> uriList = new List<String>();

            int counter = 0;
            string line = string.Empty;

            System.IO.StreamReader file = new System.IO.StreamReader(Properties.Settings.Default.LogFileDirectory + "\\" + Properties.Settings.Default.ConfigFileDirectory + "\\" + Properties.Settings.Default.URLFileName);
            while ((line = file.ReadLine()) != null)
            {
                uriList.Add(line.Trim());
                counter++;
            }

            file.Close();            

            foreach (string uri in uriList)
            {
                URITest.GetPage(uri);
            }
        }

        public void SetTimerEmail(int interval)
        {
            // Set up a timer to trigger
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = interval;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimerEmail);
            timer.Start();
        }

        public void OnTimerEmail(object sender, System.Timers.ElapsedEventArgs args)
        {
           Mail.SendLog();            
        }
    }
}
