﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PLIS_Check.Library
{
    static class ErrorLog
    {
        public static void WriteErrorLog(string message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(Properties.Settings.Default.LogFileDirectory + "\\" + Properties.Settings.Default.LogFileName, true);
                sw.WriteLine(DateTime.Now.ToString() + ": " + message);
                sw.Flush();
                sw.Close();
                sw.Dispose();
            }
            catch (Exception e)
            {
                string errorMsg = e.Message;
            }
        }
    }
}
