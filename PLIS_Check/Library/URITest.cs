﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace PLIS_Check.Library
{
    static class URITest
    {
        public static void GetPage(String url)
        {
            try
            {
                // Creates an HttpWebRequest for the specified URL. 
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                // Sends the HttpWebRequest and waits for a response.
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

                if (myHttpWebResponse.StatusCode == HttpStatusCode.OK)
                    ErrorLog.WriteErrorLog("Response Status Code for " + url + " is: " + myHttpWebResponse.StatusDescription);
                // Releases the resources of the response.
                
                myHttpWebResponse.Close();

            }
            catch (WebException e)
            {
                ErrorLog.WriteErrorLog("WebException Raised for " + url + ". The following error occured : " + e.Status);                

            }
            catch (Exception e)
            {
                ErrorLog.WriteErrorLog("The following Exception was raised for " + url + ": " + e.Message);
            }
        }
    }
}
