﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace PLIS_Check.Library
{
    static class Mail
    {
        public static void SendLog()
        {
            Timers timer = new Timers();
            System.Net.Mail.SmtpClient mySMTP = new System.Net.Mail.SmtpClient(Properties.Settings.Default.SMTPServer);
            System.Net.Mail.MailMessage myMessage = new System.Net.Mail.MailMessage();

            try
            {
                myMessage.IsBodyHtml = true;

                List<String> addressList = new List<String>();

                int counter = 0;
                string line = string.Empty;
                               
                System.IO.StreamReader file = new System.IO.StreamReader(Properties.Settings.Default.LogFileDirectory + "\\" + Properties.Settings.Default.ConfigFileDirectory + "\\" + Properties.Settings.Default.EmailFileName);
                while ((line = file.ReadLine()) != null)
                {
                    addressList.Add(line.Trim());
                    counter++;
                }

                file.Close();               

                foreach (string address in addressList)
                {
                    myMessage.To.Add(address);
                }

                myMessage.Subject = Properties.Settings.Default.EmailSubject;
                myMessage.Body = Properties.Settings.Default.EmailBody;
                myMessage.From = new System.Net.Mail.MailAddress(Properties.Settings.Default.EmailFrom);
                myMessage.Attachments.Add(new System.Net.Mail.Attachment(Properties.Settings.Default.LogFileDirectory + "\\" + Properties.Settings.Default.LogFileName));

                mySMTP.Send(myMessage);

            }
            catch (Exception ex)
            {
                myMessage.Dispose();
                ErrorLog.WriteErrorLog("Error sending mail. The following error occured : " + ex.Message);
            }
            finally
            {
                myMessage.Dispose();
                mySMTP.Dispose();
                FileOperations.moveLogFile();                
            }
        }
    }
}
