﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PLIS_Check.Library
{
    static class FileOperations
    {
        public static void checkLogDirectory()
        {
            string logDirectory = @Properties.Settings.Default.LogFileDirectory;

            if (!Directory.Exists(logDirectory))
            {
                Directory.CreateDirectory(logDirectory);
            }
        }

        public static void checkLogStoreDirectory()
        {
            string logStoreDirectory = @Properties.Settings.Default.LogFileDirectory + "\\" + Properties.Settings.Default.LogFileStore;

            if (!Directory.Exists(logStoreDirectory))
            {
            Directory.CreateDirectory(logStoreDirectory);
            }
        }

        public static void checkConfigDirectory()
        {
            string configDirectory = @Properties.Settings.Default.LogFileDirectory + "\\" + Properties.Settings.Default.ConfigFileDirectory;

            if (!Directory.Exists(configDirectory))
            {
                Directory.CreateDirectory(configDirectory);
            }

        }

        public static void checkUriFile()
        {
            string uriFile = @Properties.Settings.Default.LogFileDirectory + "\\" + Properties.Settings.Default.ConfigFileDirectory + "\\" + Properties.Settings.Default.URLFileName;

            if (!File.Exists(uriFile))
            {
                StreamWriter sw = null;
                try
                {
                    sw = new StreamWriter(uriFile, true);
                    sw.Flush();
                    sw.Close();
                    sw.Dispose();
                }
                catch (Exception e)
                {
                    string errorMsg = e.Message;
                }
            }
        }

        public static void checkEmailFile()
        {
            string emailFile = @Properties.Settings.Default.LogFileDirectory + "\\" + Properties.Settings.Default.ConfigFileDirectory + "\\" + Properties.Settings.Default.EmailFileName;

            if (!File.Exists(emailFile))
            {
                StreamWriter sw = null;
                try
                {
                    sw = new StreamWriter(emailFile, true);
                    sw.Flush();
                    sw.Close();
                    sw.Dispose();
                }
                catch (Exception e)
                {
                    string errorMsg = e.Message;
                }
            }
        }

        public static void moveLogFile()
        {
            if (File.Exists(Properties.Settings.Default.LogFileDirectory + "\\" + Properties.Settings.Default.LogFileName))
            {
                try
                {
                    string dateNow = DateTime.Now.ToString().Replace(':', '-');
                    dateNow = dateNow.Replace('/', '-');
                    dateNow = dateNow.Replace(' ', '_');
                    string sourceFile = Path.Combine(Properties.Settings.Default.LogFileDirectory, Properties.Settings.Default.LogFileName);
                    string destinationFile = Path.Combine(Properties.Settings.Default.LogFileDirectory + "\\" + Properties.Settings.Default.LogFileStore, Properties.Settings.Default.LogFileName.Replace(".txt", "_") + dateNow + ".txt");
                    File.Move(sourceFile, destinationFile);

                    //Create a new log file
                    ErrorLog.WriteErrorLog("-----------FILE CREATED-------------");
                }
                catch (Exception e)
                {
                    ErrorLog.WriteErrorLog("Error moving the Log file: " + e.Message);
                }
            }
        }

    }
}
